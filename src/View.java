import java.util.Scanner;

public class View {
	
	private final String wrongMessage = "Wrong input";
	private final String typeWordMessage = "Please type word";
	
	public void RenderWrongInput()
	{
		PrintMessageToConsole(wrongMessage);
	}
	
	public void RenderTypeWord()
	{
		PrintMessageToConsole(typeWordMessage);
	}
	
	public void RenderMessage(String message)
	{
		PrintMessageToConsole(message);
	}
	
	private void PrintMessageToConsole(String message)
	{
		System.out.println(message);
	}
}
