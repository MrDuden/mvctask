import java.util.Scanner;

public class Controller {
	
	private HelloWorldMessageModel model;
	private View view;
	
	public Controller(HelloWorldMessageModel model, View view)
	{
		this.model = model;
		this.view = view;
	}
	
	public void GetUserMessageInput()
	{
		while(!model.IsMessageValid())
		{
			view.RenderTypeWord();
			Scanner scanner = new Scanner(System.in);
			String userInput = scanner.nextLine();
			
			if(model.TryAddMessage(userInput))
			{
				if(model.IsMessageValid())
				{
					view.RenderMessage(model.GetMessage());
				}
			}
			else
			{
				view.RenderWrongInput();
			}
		}
	}
}
