
public class HelloWorldMessageModel {
	
	private final String finalMessage = "Hello world";
	private final String hello = "Hello";
	private final String world = "world";
	private String message = "";
	
	public boolean TryAddMessage(String message)
	{
		if(!IsHelloAdded() && message.contains(hello))
		{
			this.message = hello;
			return true;
		}
		else if(IsHelloAdded() && message.contains(world))
		{
			this.message += " " + world;
			return true;
		}
		
		return false;	
	}
	
	private boolean IsHelloAdded()
	{
		if(this.message == hello)
		{
			return true;
		}
		
		return false;
	}
	
	public boolean IsMessageValid()
	{
		if(this.message.equals(finalMessage))
		{
			return true;
		}
		
		return false;
	}
	
	public String GetMessage()
	{
		return message;
	}
}
