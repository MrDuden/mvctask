
public class Main {

	public static void main(String[] args) {
		HelloWorldMessageModel model = new HelloWorldMessageModel();
		View view = new View();
		Controller controller = new Controller(model, view);
		
		controller.GetUserMessageInput();
	}
}